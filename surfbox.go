package main

import (
	"bytes"
	//"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

var surfClient = &http.Client{
	Timeout: time.Second * 60,
	//Transport: &http.Transport{
	//	TLSNextProto: map[string]func(string, *tls.Conn) http.RoundTripper{},
	//},
}

var surfAPI = "https://sc17.automation.surf.net/sc17/"

type (
	SURFPort struct {
		Node string `json:"node,omitempty"`
		Port string `json:"port,omitempty"`
	}

	SURFRedirect struct {
		SURFPort
		IngressNFV bool `json:"ingress_nfv"`
	}

	SURFLimit struct {
		SURFPort
		PolicerBandwidth int `json:"policer_bandwidth"`
	}

	SURFSyncMessage struct {
		DesiredRate int
		Redirect    bool
	}
)

var surfPorts = map[string]SURFPort{
	"stitch-521": {"router1", "xe-0/2/1"},
	"stitch-520": {"router4", "xe-0/2/1"},
}

func surfCall(method, path string, v interface{}) error {
	log.Println(method, "call to", path)
	var body io.Reader
	if method != "GET" {
		if j, err := json.Marshal(v); err != nil {
			return err
		} else {
			log.Println("Body:", string(j))
			body = bytes.NewBuffer(j)
		}
	}
	req, err := http.NewRequest(method, surfAPI+path, body)
	if err != nil {
		return err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", "host-netctl/0.1")
	req.Header.Set("Content-Type", "application/json")
	resp, err := surfClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	log.Println("Response:", resp.Status)
	payload, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if len(payload) > 0 {
		log.Println("<<", string(payload))
	}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return fmt.Errorf("Bad status %s", resp.Status)
	}
	return nil
}

type SURFBox struct {
	local *HostLocal

	Rate                int
	HaveNFV, HaveFilter bool

	com chan SURFSyncMessage
}

func NewSURFBox() *SURFBox {
	sb := &SURFBox{
		local:      &HostLocal{},
		Rate:       0,
		HaveNFV:    false,
		HaveFilter: false,
		com:        make(chan SURFSyncMessage),
	}
	go sb.sync()
	return sb
}

func (s *SURFBox) sync() {
	var state SURFSyncMessage
	state.DesiredRate = -1
	state.Redirect = true
	var next SURFSyncMessage
	refresh := time.NewTicker(time.Second * 30)
	for {
		select {
		case <-refresh.C:
		case v, ok := <-s.com:
			if !ok {
				return
			}
			next.DesiredRate = v.DesiredRate
			next.Redirect = v.Redirect
		}
		if state.DesiredRate != next.DesiredRate {
			if err := surfCall("PUT", "filter", SURFLimit{SURFPort: SURFPort{Node: "router1"}, PolicerBandwidth: next.DesiredRate}); err != nil {
				log.Println("Router1 failed:", err)
				continue
			}
			if err := surfCall("PUT", "filter", SURFLimit{SURFPort: SURFPort{Node: "router4"}, PolicerBandwidth: next.DesiredRate}); err != nil {
				log.Println("Router4 failed:", err)
				continue
			}
			log.Println("Successfully changed desired rate to", next.DesiredRate)
			state.DesiredRate = next.DesiredRate
		}
		if state.Redirect != next.Redirect {
			if err := surfCall("PUT", "filter", SURFRedirect{SURFPort: SURFPort{Node: "router3"}, IngressNFV: next.Redirect}); err != nil {
				log.Println("Router3 failed:", err)
				continue
			}
			log.Println("Successfully changed redirect to", next.Redirect)
			state.Redirect = next.Redirect
		}
	}
}

func (s *SURFBox) IfaceEgressPolicer(reqs IfaceEgressPolicer) error {
	report := map[string]string{}
	desired := 0
	for _, iface := range cfg.Interfaces {
		if iface.Neighbor == "" {
			continue
		}
		bw := 0
		rate := reqs[iface.Ifname]
		if rate != "" && validTCRate(rate) {
			report[iface.Ifname] = rate
			bw, _ = strconv.Atoi(rate[:len(rate)-4])
		} else {
			report[iface.Ifname] = ""
		}
		_, ok := surfPorts[iface.Ifname]
		if !ok {
			log.Println("Attempted to set undefined port:", iface.Ifname)
			continue
		}
		desired = bw // XXX
		//if err := surfCall("PUT", "filter", SURFLimit{SURFPort: SURFPort{Node: port.Node},
		//	PolicerBandwidth: bw}); err != nil {
		//	log.Println(iface.Ifname, "failed:", err)
		//}
	}
	s.Rate = desired
	s.com <- SURFSyncMessage{DesiredRate: s.Rate, Redirect: s.HaveFilter || s.HaveNFV}
	publishr("vm/egress-policer", report)
	return nil
}

func (s *SURFBox) IfaceFilter(reqs IfaceFilter) error {
	// This would require deploying a local firewall as well;
	//s.HaveFilter = len(reqs) > 0
	s.com <- SURFSyncMessage{DesiredRate: s.Rate, Redirect: s.HaveNFV || s.HaveFilter}
	return nil
}

func (s *SURFBox) SDNRedirect(req SDNRedirect) error {
	// We can't really redirect traffic to another domain without tunneling
	return nil
}

func (s *SURFBox) NFVConfig(req NFVConfig) error {
	// TODO: in this case we need to redirect the USB iif devices

	s.HaveNFV = len(req) > 0
	s.com <- SURFSyncMessage{DesiredRate: s.Rate, Redirect: s.HaveNFV || s.HaveFilter}

	//s.state.IngressNFV = len(req) > 0
	return s.local.NFVConfig(req)
	//return s.sync()
}

func (s *SURFBox) ContainerConfig(req ContainerConfig) error {
	return s.local.ContainerConfig(req)
}

/*
func (s *SURFBox) setRate(name string, rate int) {
	cur, ok := s.state[name]
	if !ok {
		cur.SURFPort = surfPorts[name]
	}
	cur.PolicerBandwidth = rate
	s.state[name] = cur
}

func (s *SURFBox) setRedir(name string, redir bool) {
	cur, ok := s.state[name]
	if !ok {
		cur.SURFPort = surfPorts[name]
	}
	cur.IngressNFV = redir
	s.state[name] = cur
}
*/
