package main

import (
	"encoding/json"
	"log"
	"strings"
	"sync"
)

// Commands
type (
	// Rate limit traffic on an interface (map iface => rate)
	IfaceEgressPolicer map[string]string

	// TODO: probably better not to do this per interface in the future?
	Filter struct {
		Ifname string `json:"ifname"`
		Source string `json:"src"`
		Dest   string `json:"dst"`
		Proto  string `json:"proto"`
	}
	IfaceFilter []Filter

	// Redirect traffic to a specific neighbor
	RedirectEntry struct {
		Redirect string   `json:"redirect"`
		RedirTo  string   `json:"redir_to"`
		Src      []string `json:"src"`
		Dst      string   `json:"dst"`
	}
	SDNRedirect []RedirectEntry

	NFVContainer struct {
		Domain string            `json:"domain"`
		Kind   string            `json:"kind"`
		Src    []string          `json:"src"`
		Env    map[string]string `json:"env"`
	}
	NFVConfig []NFVContainer

	ContainerConfig struct {
		Prefix     string       `json:"pfx"`
		Containers []CtrRequest `json:"cs"`
	}
)

type Commander interface {
	IfaceEgressPolicer(reqs IfaceEgressPolicer) error
	IfaceFilter(reqs IfaceFilter) error
	SDNRedirect(req SDNRedirect) error
	NFVConfig(req NFVConfig) error
	ContainerConfig(req ContainerConfig) error
}

type Commands struct {
	sync.Mutex
	do Commander
}

func (c *Commands) handler(k, v []byte) {
	c.Lock()
	defer c.Unlock()
	var err error
	parts := strings.Split(string(k), "/")
	switch parts[2] {
	case "policer":
		var req IfaceEgressPolicer
		if err = json.Unmarshal(v, &req); err == nil {
			err = c.do.IfaceEgressPolicer(req)
		}
	case "iface-filter":
		var req IfaceFilter
		if err = json.Unmarshal(v, &req); err == nil {
			err = c.do.IfaceFilter(req)
		}
	case "tunnel":
		// TODO
	case "sdn-redirect":
		var req SDNRedirect
		if err = json.Unmarshal(v, &req); err == nil {
			err = c.do.SDNRedirect(req)
		}
	// Regular containers, including attack scenarios
	case "containers":
		// TODO: if we split this into multiple subsections, we can use retain
		// to survive race conditions
		var req ContainerConfig
		if err = json.Unmarshal(v, &req); err == nil {
			err = c.do.ContainerConfig(req)
		}
	// NFV containers
	case "nfv":
		var req NFVConfig
		if err = json.Unmarshal(v, &req); err == nil {
			err = c.do.NFVConfig(req)
		}
	default:
		log.Println("Unknown command:", string(k))
	}
	if err != nil {
		log.Println("host-netctl: Error", string(k), "->", err)
	}
}
