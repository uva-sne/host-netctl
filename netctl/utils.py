from subprocess import check_output, Popen, PIPE
from tempfile import NamedTemporaryFile
from os import chmod, rename, unlink
from os.path import exists, dirname


def parse_envlines(lines):
    return dict([v.strip() for v in line.split('=', 1)]
                for line in lines
                if not line.startswith('#') and '=' in line)


def ip_to_iface():
    table = {}
    ifaces = set()
    addr = check_output(['ip', 'addr', 'ls']).decode('utf8')
    iface = None
    for line in addr.split('\n'):
        if not line:
            continue
        parts = line.split()
        if not line.startswith(' '):
            iface = parts[1].rstrip(':')
            ifaces.add(iface)
        #elif len(parts) > 1 and parts[0] == 'link/ether':
        #    iface_hw = parts[1]
        elif len(parts) > 1 and parts[0] == 'inet':
            table[parts[1].rsplit('/', 1)[0]] = iface
    return table, ifaces


def save(fn, data):
    """Atomic replace"""
    tmp = NamedTemporaryFile(mode='w', dir=dirname(fn), delete=False)
    try:
        tmp.write(data)
        tmp.flush()
        chmod(tmp.name, 0o644)
        rename(tmp.name, fn)
        tmp.close()
    finally:
        if exists(tmp.name):
            unlink(tmp.name)


def run_with_input(cmd, input):
    with Popen(cmd, stdin=PIPE) as proc:
        return proc.communicate(input.encode('utf8'))
