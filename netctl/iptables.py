

def parse_iptables(listing):
    tables = {}
    table = {}
    cname = 'BAD_DATA'
    for line in listing.split('\n'):
        if line.startswith('#'):
            continue
        elif line.startswith('*'):
            tables[line[1:]] = table = {}
            cname = 'BAD_DATA'
            continue
        elif line.startswith(':'):
            parts = line.split()
            cname = parts[0][1:]
            policy = parts[1]
            table[cname] = (policy, [])
        elif line.startswith('-A '):
            parts = line.split(' ', 2)
            _, chain = table[parts[1]]
            chain.append(parts[2].rstrip())
        elif line and line != 'COMMIT':
            raise ValueError(line)
    return tables


def restore_iptables(tables):
    rule_blob = ''
    for table, chains in tables.items():
        rule_blob += '*' + table + '\n'
        for chain, (policy, _) in sorted(chains.items()):
            rule_blob += ':' + chain + ' ' + policy + ' [0:0]\n'
        for chain, (_, rules) in sorted(chains.items()):
            for rule in rules:
                rule_blob += '-A ' + chain + ' ' + rule + '\n'
        rule_blob += 'COMMIT\n'
    #run('iptables-restore', input=rule_blob)
    return rule_blob
