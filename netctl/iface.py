#!/usr/bin/env python3
from __future__ import print_function
from subprocess import check_call, call
from os import devnull

# GRE?
VALID_FILTER_PROTO = ('none', 'icmp', 'udp', 'tcp')

filter_drop_rule = '-o %s -p %s -j DROP'


def export_iptables(firewall_rules):
    iptables = '*filter\n'
    iptables += ':INPUT ACCEPT [0:0]\n'
    iptables += ':OUTPUT ACCEPT [0:0]\n'
    iptables += ':FORWARD ACCEPT [0:0]\n'
    for iface, rules in sorted(firewall_rules.items()):
        for rule in rules:
            if rule['c'] == 'OUTPUT':
                dir = '-o'
            else:
                dir = '-i'
            adv = ''
            if 's' in rule:
                adv += ' -s ' + rule['s']
            if 'd' in rule:
                adv += ' -d ' + rule['d']
            iptables += '-A %s %s %s -p %s%s -j %s\n' % (rule['c'], dir, iface,
                                                         rule['p'], adv,
                                                         rule['j'])
    iptables += 'COMMIT\n'
    return iptables


def link_filter(firewall_rules, iface, filter_protos):
    """
    TODO: report back rules
    """

    new_rules = []
    iface_rules = firewall_rules.get(iface, [])
    for rule in iface_rules:
        if rule['j'] == 'DROP':
            continue
        new_rules.append(rule)
    for v in filter_protos:
        base = {'j': 'DROP'}
        if isinstance(v, dict):
            base['p'] = v['p']
            if 's' in v:
                base['s'] = v['s']
            if 'd' in v:
                base['d'] = v['d']
        else:
            base['p'] = v
        for k in ('FORWARD', 'OUTPUT', 'INPUT'):
            rule = {'c': k}
            rule.update(base)
            new_rules.append(rule)
    firewall_rules[iface] = new_rules


def link_filter_flush(firewall_rules):
    for k in firewall_rules.keys():
        del firewall_rules[k]


def link_state(iface, state):
    """TODO: no longer used"""
    pass
    '''
    state = state.lower()
    if state not in ('up', 'down'):
        exit(1)
    if iface in ('lo', 'eth0'):
        print('Setting state of', iface, 'prohibited')
        exit(2)
    check_call(['ip', 'link', 'set', iface, state])
    '''


def link_egress(iface, rate):
    with open(devnull, 'r') as n:
        call(['tc', 'qdisc', 'del', 'dev', iface, 'root'], stderr=n)
        if rate not in ('inf', 'max'):
            check_call(['tc', 'qdisc', 'add', 'dev', iface, 'root', 'tbf',
                        'rate', rate,
                        'limit', '10m',
                        'burst', rate], stderr=n)
