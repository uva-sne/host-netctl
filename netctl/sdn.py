from subprocess import call, check_output, run

from .utils import ip_to_iface

# TODO:
# * Atomic replace
# * Determine NFV MAC
# * Determine router/service ports

# TODO: We could get MACs from "brctl showmacs br0", but ageing might have
# expired it

#config = parse_envlines(open('/etc/vnet-config'))


def sdn_chain_interfaces(config, nfv_mac):
    call(['ebtables', '-t', 'nat', '-F'])
    call(['ebtables', '-t', 'nat', '-A', 'PREROUTING', '-p', 'ARP',
          '-j', 'ACCEPT'])

    '''
    for k, v in config.items():
        if not k.startswith('iface_'):
            continue
        parts = v.split()
        #if not parts[-1].startswith('nfv'):
        if 'nfv' not in parts[-1]:
            continue
        nfv_iface_ip = k[6:].replace('_', '.')
        break
    else:
        exit(1)

    ip_lut, ifaces = ip_to_iface()
    nfv_iface = ip_lut.get(nfv_iface_ip)
    if not nfv_iface:
        exit(2)

    print('nfv', nfv_iface_ip, nfv_iface)
    for iface in ifaces:
        if not iface.startswith('eth') or iface in ('eth0', nfv_iface):
            continue
        call(['ebtables', '-t', 'nat', '-A', 'PREROUTING',
              '-i', iface,
              '-j', 'dnat', '--to-dst', nfv_mac, '--dnat-target', 'ACCEPT'])
    '''

def sdn_chain(nfv_conf, functions):
    tables = parse_ebtables(check_output(['ebtables', '-t', 'nat', '-L']))
    chain = tables[b'nat'][b'PREROUTING ACCEPT']
    new_chain = []

    for c in chain:
        if b'-j dnat' in c:
            continue
        new_chain.append(c)

    # In fact, we should consider doing -p IPv4 on the other rules instead
    arp_rule = b'-A PREROUTING -p ARP -j ACCEPT'
    if not new_chain or new_chain[0] != arp_rule:
        print('Adding ARP rule', new_chain)
        new_chain = [arp_rule] + new_chain

    base_rule = b'-A PREROUTING -i %s%s -j dnat --to-dst %s --dnat-target ACCEPT'
    last_iface = 'eth1'
    for func in functions:
        args = func.split(':')
        cname = args.pop(0)
        container = nfv_conf[cname]

        if args:
            for ip in args:
                new_chain.append(base_rule % (last_iface.encode('utf8'),
                                              b' -p IPv4 --ip-src ' +
                                              ('10.100.' + ip).encode('utf8'),
                                              container['mac'].encode('utf8')))
                new_chain.append(base_rule % (last_iface.encode('utf8'),
                                              b' -p IPv4 --ip-dst ' +
                                              ('10.100.' + ip).encode('utf8'),
                                              container['mac'].encode('utf8')))
        else:
            new_chain.append(base_rule % (last_iface.encode('utf8'), b'',
                                          container['mac'].encode('utf8')))

        if not args:
            # XXX: chains with arguments can't the last interface
            last_iface = container['iface']

    if chain == new_chain:
        print('Chain does not need updating')
        return

    tables[b'nat'][b'PREROUTING ACCEPT'] = new_chain
    restore_ebtables(tables)
    #save('/tmp/metadata/nfv-host.json', dumps({'nfv-chain': functions}))


def parse_ebtables(listing):
    tables = {}
    table = {}
    chain = []
    cname = b'BAD_DATA'
    for line in listing.split(b'\n'):
        parts = line.split()
        if line.startswith(b'Bridge table:'):
            tables[parts[2]] = table = {}
            chain = []
            cname = b'BAD_DATA'
        elif line.startswith(b'Bridge chain:'):
            cname = parts[2].rstrip(b',')
            policy = parts[-1]
            table[cname + b' ' + policy] = chain = []
        elif line:
            chain.append(b'-A ' + cname + b' ' + line.rstrip())
    return tables


def restore_ebtables(tables):
    rule_blob = b''
    for table, chains in tables.items():
        rule_blob += b'*' + table + b'\n'
        for chain, _ in chains.items():
            rule_blob += b':' + chain + b'\n'
        for _, rules in chains.items():
            for rule in rules:
                rule_blob += rule + b'\n'
        rule_blob += b'\n'
    run('ebtables-restore', input=rule_blob)
