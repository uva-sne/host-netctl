package main

import (
	"fmt"

	"vnet.uvalight.net/utils"
)

type rules []string

// table -> chain -> rule
var iptables map[string]map[string]rules = map[string]map[string]rules{
	"filter": map[string]rules{
		"INPUT":   nil,
		"FORWARD": nil,
	},
}

func iptablesDump() string {
	blob := ""
	for table, chains := range iptables {
		blob += fmt.Sprint("*", table, "\n")
		for name, _ := range chains { // sorted
			//blob += fmt.Sprint(":", name, " ", chain.policy, " [0:0]\n")
			blob += fmt.Sprint(":", name, " ACCEPT [0:0]\n")
		}
		for name, rules := range chains { // sorted
			for _, rule := range rules {
				blob += fmt.Sprintln("-A", name, rule)
			}
		}
		blob += "COMMIT\n"
	}
	return blob
}

func iptablesSave() {
	s := iptablesDump()
	utils.RunLogged("host-netctl", "iptables-restore", nil, &s)
}
