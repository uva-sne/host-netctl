package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"os/signal"
	"strings"

	"vnet.uvalight.net/utils"
	"vnet.uvalight.net/utils/mqtt"
)

var (
	//controlPath = flag.String("control-path", "/tmp/vnet/netctl", "")
	ifaces = flag.String("interfaces", "lo,eth0", "protected interfaces")

	surf = flag.Bool("surf", false, "SURF mode")
	mini = flag.Bool("mini", false, "Mac mini mode")
)

func interrupt(c chan os.Signal, cancel chan struct{}) {
	<-c
	close(cancel)
	signal.Reset(os.Interrupt)
}

var cfg *utils.Config

func main() {
	utils.Syslog("host-netctl")
	log.Println("host-netctl: Start")

	flag.Parse()

	monitorIfaces := map[string]bool{}
	cfg = utils.LoadConfig()
	for _, iface := range cfg.Interfaces {
		if iface.Neighbor == "" {
			interfaceIgnore[iface.Ifname] = true
		} else if !strings.HasPrefix(iface.Ifname, "stitch-") {
			monitorIfaces[iface.Ifname] = true
		}
	}

	cancel := make(chan struct{}, 1)
	sig := make(chan os.Signal, 1)
	go interrupt(sig, cancel)
	signal.Notify(sig, os.Interrupt)

	// <Reset or import state>
	cli, err := mqtt.New("host-netctl")
	if err != nil {
		log.Fatalln("MQTT setup:", err)
	}
	defer cli.Close()

	ctrRegistry = cfg.Items["docker_registry"]
	ctrVersion = cfg.Items["docker_container_version"]
	ctrInit(cfg.Items["vnet_routing_asn"], msgs)

	/* Publish ifmap */
	publishIfmap(cli)

	/* - Publish interface state
	 * - Publish interface PPS/BPS
	 * - Publish flow information
	 * - <Publish netctl state>
	 */
	go publisher(cli, msgs)

	if !*surf {
		newInterfaceMonitor(monitorIfaces)
		newFlowMonitor(monitorIfaces)
		newNetworkMonitor(monitorIfaces)
	}

	// Deal with incoming commands
	var c *Commands
	if *surf {
		/* - Multiple USB uplinks
		 */
		c = &Commands{do: NewSURFBox()}
	} else if *mini {
		// TODO
		c = &Commands{do: &HostLocal{}}
	} else {
		c = &Commands{do: &HostLocal{}}
	}
	// First reset all state
	if err := c.do.IfaceEgressPolicer(nil); err != nil {
		log.Println("Failed to reset policer:", err)
	}
	if err := c.do.IfaceFilter(nil); err != nil {
		log.Println("Failed to reset filter:", err)
	}
	cli.Sub("cmd/host-netctl/#", c.handler)

	<-cancel
}

func publishIfmap(cli *mqtt.MQTT) {
	ifmap := map[string]string{}
	for _, v := range cfg.Interfaces {
		if v.Neighbor != "" {
			ifmap[v.Ifname] = v.Neighbor
		}
	}
	b, err := json.Marshal(ifmap)
	if err != nil {
		log.Fatalln("Failed to produce ifmap:", err)
	}
	if err := cli.PubRetain("vm/ifmap", string(b)); err != nil {
		log.Fatalln("Failed to publish ifmap:", err)
	}
}
