// Aggregated flow monitoring
package main

import (
	"log"
	"os"
	"os/exec"
	"time"
)

type FlowBundle []string

type FlowMon struct {
	ifaces map[string]bool
}

func newFlowMonitor(ifaces map[string]bool) *FlowMon {
	fm := &FlowMon{ifaces}
	go fm.monitor()
	return fm
}

// TODO: the process can crash and we won't revive it
// TODO: keep retrying with backoff
func (fm *FlowMon) monitor() {
	if len(fm.ifaces) == 0 {
		return
	}

	var c *exec.Cmd
	var exited chan struct{}
	var ifaces []string
	for k, _ := range fm.ifaces {
		ifaces = append(ifaces, k)
	}

	for {
		atleast := time.Now().Add(time.Second * 5)
		c, exited = flowAggr(ifaces)
		<-exited
		if c != nil && c.Process != nil {
			c.Process.Kill()
		}
		if time.Now().Before(atleast) {
			time.Sleep(time.Second * 5)
		}
	}
}

// Helper that parses flow-aggr output and relays it to the channel
type bundleRelay struct {
	buf []byte
	cur FlowBundle
}

func flowAggr(ifaces []string) (*exec.Cmd, chan struct{}) {
	log.Println("flow-aggr:", ifaces)
	c := exec.Command("flow-aggr", ifaces...)
	c.Stdout = &bundleRelay{nil, nil}
	c.Stderr = os.Stderr
	if err := c.Start(); err != nil {
		log.Println("flow-aggr failed:", err)
		return nil, nil
	}
	exited := make(chan struct{})
	go func() {
		defer close(exited)
		err := c.Wait()
		log.Println("flow-aggr exited:", err)
	}()
	return c, exited
}

// NB: writes should typically end in newline
func (br *bundleRelay) Write(b []byte) (int, error) {
	var buf []byte
	if br.buf != nil {
		buf = append(br.buf, b...)
	} else {
		buf = b
	}

	// Scan for marker
	offz := 0
	for i, c := range buf {
		if c == '\n' {
			s := string(buf[offz:i])
			offz = i + 1
			if s == "*" {
				publish("vm/fs", br.cur) // TODO
				br.cur = nil
			} else {
				br.cur = append(br.cur, s)
			}
		}
	}
	if len(buf) > offz {
		br.buf = buf[offz:]
	} else {
		br.buf = nil
	}
	return len(b), nil
}
