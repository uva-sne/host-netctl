package main

import (
	"time"
	"vnet.uvalight.net/coms"

	procinfo "github.com/c9s/goprocinfo/linux"
)

// Bandwidth

type NetworkStats struct {
	ifaces map[string]bool
	clock  *time.Ticker

	prev coms.IfaceRateBundle
	at   int64
}

func newNetworkMonitor(ifaces map[string]bool) *NetworkStats {
	var ival = time.Second * 2
	ns := &NetworkStats{
		ifaces,
		time.NewTicker(ival),
		collectNetworkStats(ifaces),
		time.Now().UnixNano(),
	}
	go timer(ns.clock, ns.monitor)
	return ns
}

func (ns *NetworkStats) monitor() {
	cur := collectNetworkStats(ns.ifaces)
	at := time.Now().UnixNano()
	tdf := float64(at-ns.at) / 1e9
	stats := cur.Delta(ns.prev, tdf)
	ns.prev = cur
	ns.at = at
	publish("vm/ns", stats) // TODO
}

func collectNetworkStats(ifaces map[string]bool) (ns coms.IfaceRateBundle) {
	rows, err := procinfo.ReadNetworkStat("/proc/net/dev")
	if err != nil {
		panic(err)
	}
	ns = make(coms.IfaceRateBundle)
	for _, row := range rows {
		if !ifaces[row.Iface] {
			continue
		}
		ns[row.Iface] = coms.IfaceRate{
			coms.Rate{row.RxBytes, row.RxPackets},
			coms.Rate{row.TxBytes, row.TxPackets},
		}
	}
	return
}
