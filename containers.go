package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"strings"

	"vnet.uvalight.net/utils"
)

type CtrRequest struct {
	Name  string            `json:"name"`
	Image string            `json:"img"`
	Net   string            `json:"net"`
	IP    string            `json:"ip"`
	Priv  bool              `json:"priv"`
	Env   map[string]string `json:"env"`
	Pull  bool              `json:"pull"`
}

var (
	ctrRegistry = ""
	ctrVersion  = ""
)

var (
	ErrNotFound = errors.New("Container not found")
)

// TODO:
// - Report running containers

// state:
// - Which containers are desired?
//
// NFV rule input
// - user rules
// - container (dis)connect: gives IP

// Monitor container status
type DockerMonitor struct {
	msgs   chan M
	Create chan CtrRequest
}

func NewDockerMonitor(msgs chan M) *DockerMonitor {
	dm := &DockerMonitor{msgs, make(chan CtrRequest)}
	// TODO: retry
	go func() {
		err := dm.monitor()
		if err != nil {
			log.Println("Docker monitor exited:", err)
		}
	}()
	return dm
}

type DockerMadness struct {
	Id     string `json:"id"`     // Container ID
	Type   string `json:"Type"`   // container, network
	Action string `json:"Action"` // create/connect/...

	Actor struct {
		Attributes map[string]string `json:"Attributes"`
		// .name (container events)
		// .container
	} `json:"Actor"`
}

func (dm *DockerMonitor) monitor() error {
	c := exec.Command("docker", "events", "--format", "{{json .}}")
	o, err := c.StdoutPipe()
	if err != nil {
		return err
	}
	defer o.Close()
	scanner := bufio.NewScanner(o)
	if err := c.Start(); err != nil {
		return err
	}

	// TODO: must parse [docker ps] here to avoid races
	containers := map[string]string{}
	for scanner.Scan() {
		var m DockerMadness
		line := scanner.Text()
		if err := json.Unmarshal([]byte(line), &m); err != nil {
			log.Println("docker events:", err)
			continue
		}
		if m.Type == "container" {
			name := m.Actor.Attributes["name"]
			switch m.Action {
			case "create", "start":
				containers[name] = m.Action
			case "die", "stop", "destroy":
				delete(containers, name)
			}
			// TODO: only if updated
			dm.msgs <- M{"vm/containers", utils.JSON(containers), true}
		}
	}
	return c.Wait()
}

var (
	netPrefix  = ""
	containers = map[string]string{}
	dm         *DockerMonitor
)

func ctrInit(domain string, msgs chan M) {
	// Build container net
	netPrefix = fmt.Sprintf("172.30.%s", domain)
	dockerBridge("vc-net", netPrefix)
	dm = NewDockerMonitor(msgs)
}

func ctrAdd(req CtrRequest) (string, error) {
	image := fmt.Sprintf("%s/%s:%s", ctrRegistry, req.Image, ctrVersion)
	purge := false

	if req.Pull {
		before, _ := outrun("docker", []string{"images", "--format", "{{.ID}}", image})
		run("docker", []string{"pull", image})
		after, _ := outrun("docker", []string{"images", "--format", "{{.ID}}", image})

		// If image pull was requested and resulted in an update, always nuke
		// container
		purge = before != after
	}

	if !purge {
		purge = true
		running, _ := outrun("docker", []string{"inspect", "--format", "{{.State.Running}}", req.Name})
		log.Println("Container", req.Name, "is running:", running)
		if running == "true" {
			// The container is running, inspect its parameters
			// Verify env
			s, _ := outrun("docker", []string{"inspect", "--format", "{{json .Config.Env}}", req.Name})
			var env []string
			if err := json.Unmarshal([]byte(s), &env); err == nil {
				purge = !compareEnv(req.Env, env)
				if purge {
					log.Printf("Env mismatched: %+q %+q", env, req.Env)
				}
			} else {
				log.Println("Failed to parse env:", err)
			}
		}
	}

	if purge {
		// Either it's not running, it doesn't exist, or the params are wrong
		log.Println("Nukerino", req.Name)
		dockerNuke(req.Name)
	}
	if !purge {
		log.Println("Container", req.Name, "is already up and running")
		return ctrDiscover(req.Name)
	}

	if req.Net == "" {
		req.Net = "vc-net"
	}
	ip := ""
	args := []string{"run", "--detach", "--net", req.Net}
	if req.Priv {
		args = append(args, "--privileged")
	}
	if req.Net != "host" && req.IP != "" {
		ip = fmt.Sprintf("%s.%s", netPrefix, req.IP)
		args = append(args, "--ip", ip)
	}
	args = append(args, "--env", fmt.Sprintf("NAME=%s", req.Name))
	for k, v := range req.Env {
		args = append(args, "--env", fmt.Sprintf("%s=%s", k, v))
	}
	if req.Image != "sarnet-agent" {
		// Limit CPU use for utility containers
		// TODO: consider --cpu-shares
		args = append(args, "--cpuset-cpus=0")
	}
	args = append(args, "--name", req.Name, image)
	log.Printf("Create container: %+v", req.Name)
	err := run("docker", args)
	if err != nil {
		return ip, err
	} else if ip == "" && req.Net != "host" {
		return ctrDiscover(req.Name)
	}
	return ip, nil
}

func ctrDiscover(name string) (string, error) {
	addrs, err := outrunLines("docker", []string{"inspect", "--format",
		"{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}",
		name})
	if err != nil {
		log.Print("Failed to get IP for ", name, ": ", err)
	} else if len(addrs) != 1 {
		// No IP??
		return "", nil
	}
	return addrs[0], nil
}

func ctrDel(name string) error {
	return dockerNuke(name)
}

func dockerBridge(netname, prefix string) {
	run("docker", []string{"network", "create",
		"--driver", "bridge",
		"-o", "com.docker.network.bridge.name=" + netname,
		"--gateway", prefix + ".1",
		"--subnet", prefix + ".0/24",
		netname})
}

// NB: env contains other junk such as PATH=
func compareEnv(env map[string]string, active []string) bool {
	seen := 0
	for _, a := range active {
		c := strings.Index(a, "=")
		if c < 0 {
			return false
		}
		k := a[:c]
		v := a[c+1:]
		if new, ok := env[k]; ok {
			if new != v {
				return false
			}
			seen += 1
		}
	}
	return seen == len(env)
}

// Return running containers for a given name wildcard
func dockerRunning(namewc string) ([]string, error) {
	return outrunLines("docker", []string{"ps", "-a", "--format", "{{.Names}}",
		"--filter", "name=" + namewc})
}

func dockerNuke(name string) error {
	return run("docker", []string{"rm", "--force", name})
}
