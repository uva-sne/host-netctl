package main

import (
	"fmt"
	"net"
	"strings"
	"time"

	"vnet.uvalight.net/coms"
	"vnet.uvalight.net/utils"
)

// TODO: flags
// TODO: auto mark Docker bridges/veth?
var interfaceRoles map[string]string = map[string]string{
	"lo":   "ignore",
	"eth0": "mgmt",
}

var interfaceIgnore = map[string]bool{"eth0": true}

func interfaceRole(iface string) string {
	v, _ := interfaceRoles[iface]
	return v
}

func blacklistedIface(s string) bool {
	if !strings.HasPrefix(s, "eth") {
		return true
	}
	return interfaceIgnore[s]
}

//
// Interface status monitoring
//

type InterfaceStatus struct {
	ifaces map[string]bool
	clock  *time.Ticker
	cur    []coms.InterfaceInfo
}

func newInterfaceMonitor(ifaces map[string]bool) {
	is := &InterfaceStatus{ifaces, time.NewTicker(5 * time.Second), nil}
	go utils.Timer(is.clock, is.monitor)
}

func (is *InterfaceStatus) monitor() {
	cur := Interfaces(is.ifaces)
	if !InterfacesEqual(is.cur, cur) {
		is.cur = cur
		for _, iface := range cur {
			publishr(fmt.Sprintf("if/%s", iface.Name), iface)
		}
	}
}

func Interfaces(ifaces map[string]bool) (ii []coms.InterfaceInfo) {
	ifs, err := net.Interfaces()
	if err != nil {
		panic(err)
	}

	for _, iface := range ifs {
		if !ifaces[iface.Name] {
			continue
		}
		state := InterfaceState(iface.Name)
		rate := ""   //InterfaceRate(iface.Name)
		filter := "" // InterfaceFilter(iface.Name) // XXX?
		info := coms.InterfaceInfo{iface.Name,
			formatMAC(iface.HardwareAddr),
			state, rate, filter, []string{}, []string{}}
		addrs, err := iface.Addrs()
		if err != nil {
			panic(err)
		}
		for _, a := range addrs {
			// This is a bit silly.
			ip := a.String()
			if strings.HasPrefix(ip, "fe80:") {
				continue
			} else if strings.ContainsAny(ip, ":") {
				info.V6 = append(info.V6, ip)
			} else {
				info.V4 = append(info.V4, ip)
			}
		}
		ii = append(ii, info)
	}

	return
}

// We assume interfaces and IPs will be defined in the same order, at least
// most of the time. If not, sorting must be done.
func InterfacesEqual(left, right []coms.InterfaceInfo) bool {
	if len(left) != len(right) {
		return false
	}
	for i := 0; i < len(left); i++ {
		ok := (left[i].Name == right[i].Name) &&
			(left[i].State == right[i].State) &&
			(left[i].Rate == right[i].Rate) &&
			(left[i].Filter == right[i].Filter) &&
			(stringArrayEqual(left[i].V4, right[i].V4)) &&
			(stringArrayEqual(left[i].V6, right[i].V6))
		if !ok {
			return false
		}
	}
	return true
}

func formatMAC(b []byte) string {
	if len(b) != 6 {
		return ""
	}
	return fmt.Sprintf("%02X:%02X:%02X:%02X:%02X:%02X",
		b[0], b[1], b[2], b[3], b[4], b[5])
}
