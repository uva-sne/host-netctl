package main

import (
	"encoding/json"
	"log"

	"vnet.uvalight.net/utils/mqtt"
)

type M struct {
	Topic  string
	Value  string
	Retain bool
}

// Monitor MQTT-based metadata for a single connection attempt
func publisher(cli *mqtt.MQTT, msgs chan M) {
	for {
		m := <-msgs
		var err error
		if m.Retain {
			err = cli.PubRetain(m.Topic, m.Value)
		} else {
			err = cli.Pub(m.Topic, m.Value)
		}
		if err != nil {
			log.Fatalln("MQTT publish:", err)
		}
	}
}

var msgs = make(chan M, 16)

func publish(topic string, val interface{}) {
	v, _ := json.Marshal(val)
	msgs <- M{topic, string(v), false}
}

func publishr(topic string, val interface{}) {
	v, _ := json.Marshal(val)
	msgs <- M{topic, string(v), true}
}
