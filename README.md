# host-netctl

Control a Linux-based host to satisfy your SDN & NFV needs.


Features:

* Interface chaining
* Rate limiting
* Traffic class filtering
* Resolve Docker containers by name to interface
* vnet: Resolve neighbor by name to interface


TODO:

* Be wary of SARNET agent traffic
* Monitor interface add/remove
* Interact with vnet-redis
* Identify interfaces by MAC
* Monitor interface changes


Requirements:

* The netctl tool manages the iptables filter OUTPUT/FORWARD chains, external
  manipulation may break things


## General model

* Daemon starts and inherits or resets state
* Processes communicate via fifo (future work: secure channel) to request
  changes -- see bin/host-netctl for command formats
