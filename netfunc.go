package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"regexp"
	"strings"

	"vnet.uvalight.net/utils"
)

//
// Link state
//

// Report the active interface state
//
// Based on include/uapi/linux/if.h
func InterfaceState(iname string) string {
	body, err := ioutil.ReadFile("/sys/class/net/" + iname + "/flags")
	if err != nil {
		return ""
	}

	at := len(body) - 1
	for at > 0 {
		if body[at] != '\n' {
			break
		}
		at -= 1
	}

	if hex2dec(body[at])&1 == 1 {
		return "up"
	}

	return "down"
}

func SetInterfaceState(iname string, updown bool) bool {
	state := "up"
	if !updown {
		state = "down"
	}
	return utils.RunLogged("host-netctl", "ifconfig", []string{iname, state}, nil) == nil
}

//
// Link rate
//

// Report the active interface rate
// Can return different units (Kbit Mbit Gbit Tbit)
func InterfaceRate(iname string) string {
	c := exec.Command("tc", "qdisc", "show", "dev", iname)
	stats, err := c.CombinedOutput()
	if err != nil {
		log.Println("vnetmon :qdisc show", err)
		return ""
	}

	for _, line := range bytes.Split(stats, []byte{'\n'}) {
		parts := bytes.Split(line, []byte{' '})
		if len(parts) < 4 || !bytes.Equal(parts[0], []byte("qdisc")) || !bytes.Equal(parts[3], []byte("root")) {
			continue
		}
		for i := 4; i < (len(parts) - 1); i++ {
			if bytes.Equal(parts[i], []byte("rate")) {
				return string(parts[i+1])
			}
		}
	}

	return ""
}

func SetInterfaceRate(iname string, rate, burst int) bool {
	c := exec.Command("tc", "qdisc", "del", "dev", iname, "root")
	err := c.Run()
	if err != nil {
		log.Println("vnetmon :qdisc del", err)
		return false
	}

	c = exec.Command("tc", "qdisc", "add", "dev", iname, "root", "tbf",
		"rate", fmt.Sprintf("%dMbit", rate),
		"limit", "10m",
		"burst", fmt.Sprintf("%dMbit", burst))
	err = c.Run()
	if err != nil {
		log.Println("vnetmon :qdisc add", err)
		return false
	}

	return true
}

//
// Link filter
//
var rMultiSpaces = regexp.MustCompile(`\s+`)
var filteredProtos = []string{"udp", "tcp", "icmp"}
var filteredTables = [][]string{
	[]string{"FORWARD", "-o"},
	[]string{"INPUT", "-i"},
	[]string{"OUTPUT", "-o"}}

func InterfaceFilter(iname string) string {
	c := exec.Command("iptables", "-L", "-w", "-nv")
	stats, err := c.CombinedOutput()
	if err != nil {
		log.Println("vnetmon severity=error :iptables:", err)
		return ""
	}
	var proto []string
	for _, line := range bytes.Split(stats, []byte{'\n'}) {
		parts := rMultiSpaces.Split(strings.TrimSpace(string(line)), -1)
		if len(parts) < 9 || parts[2] != "DROP" || parts[7] != "0.0.0.0/0" || parts[8] != "0.0.0.0/0" {
			continue
		}
		if parts[5] != iname && parts[6] != iname {
			continue
		}
		p := parts[3]
		if p != "udp" && p != "tcp" && p != "icmp" {
			continue
		}
		if !stringArrayContains(proto, p) {
			proto = append(proto, p)
		}
	}
	return strings.Join(proto, ",")
}

func SetInterfaceFilter(iname, proto string) bool {
	if !stringArrayContains(filteredProtos, proto) {
		return false
	}
	result := true
	for _, v := range filteredProtos {
		for _, t := range filteredTables {
			do := "-D"
			if v == proto {
				do = "-A"
			}
			if utils.RunLogged("host-netctl", "iptables", []string{do, t[0],
				"-p", proto,
				t[1], iname,
				"-j", "DROP"}, nil) != nil {
				result = false
			}
		}
	}
	return result
}
