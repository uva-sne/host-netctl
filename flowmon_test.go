package main

import (
	"testing"
)

func TestBundleRelay(t *testing.T) {
	fb := make(chan FlowBundle, 1)
	br := &bundleRelay{fb, nil, nil}
	br.Write([]byte("test\nline\n*\nnext\n"))
	if br.buf != nil {
		t.Errorf("Expected nil buffer, not %+q", br.buf)
	}
	if len(br.cur) != 1 || br.cur[0] != "next" {
		t.Errorf("Expected one future entry, not %+q", br.cur)
	}
	select {
	case lines, ok := <-fb:
		if !ok || len(lines) != 2 || lines[0] != "test" || lines[1] != "line" {
			t.Errorf("Expected test, line, not %+q", lines)
		}
	default:
		t.Errorf("Expected result")
	}
	br.Write([]byte("*"))
	if br.buf == nil || len(br.buf) != 1 || br.buf[0] != '*' {
		t.Errorf("Expected buffer, not %+q", br.buf)
	}

	br.Write([]byte("\n"))
	if br.buf != nil {
		t.Errorf("Expected nil buffer, not %+q", br.buf)
	}
	if len(br.cur) != 0 {
		t.Errorf("Expected no future entry, not %+q", br.cur)
	}
	select {
	case lines, ok := <-fb:
		if !ok || len(lines) != 1 || lines[0] != "next" {
			t.Errorf("Expected next, not %+q", lines)
		}
	default:
		t.Errorf("Expected result")
	}
}
