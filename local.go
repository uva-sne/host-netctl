package main

import (
	"fmt"
	"log"

	"vnet.uvalight.net/utils"
)

type HostLocal struct {
}

func (c *HostLocal) IfaceEgressPolicer(reqs IfaceEgressPolicer) error {
	report := map[string]string{}
	for _, iface := range cfg.Interfaces {
		if iface.Neighbor == "" {
			continue
		}
		// TODO: don't del if rate is correct
		if err := run("tc", []string{"qdisc", "del", "dev", iface.Ifname, "root"}); err != nil {
			log.Println("Failed to reset rate on", iface.Ifname, "because", err)
		}
		rate := reqs[iface.Ifname]
		if rate != "" && validTCRate(rate) {
			if err := run("tc", []string{"qdisc", "add", "dev", iface.Ifname, "root", "tbf",
				"rate", rate,
				"limit", "10m",
				"burst", rate}); err != nil {
				log.Println("Failed to apply rate", rate, "to", iface.Ifname, "because", err)
			}
			report[iface.Ifname] = rate
		} else {
			report[iface.Ifname] = ""
		}
	}
	publish("vm/egress-policer", report)
	return nil
}

func (c *HostLocal) IfaceFilter(reqs IfaceFilter) error {
	var rules []string
	for _, req := range reqs {
		if req.Proto != "udp" && req.Proto != "tcp" && req.Proto != "icmp" {
			return fmt.Errorf("Invalid protocol: %v", req.Proto)
		}
		rule := fmt.Sprint("-p ", req.Proto)
		if req.Ifname != "" {
			rule += fmt.Sprint(" -i ", req.Ifname)
		}
		if req.Source != "" {
			rule += fmt.Sprint(" -s ", req.Source)
		}
		if req.Dest != "" {
			rule += fmt.Sprint(" -d ", req.Dest)
		}
		rule += " -j DROP"
		rules = append(rules, rule)
	}

	iptables["filter"]["INPUT"] = rules
	iptables["filter"]["FORWARD"] = rules
	iptablesSave()
	return nil
}

func (c *HostLocal) SDNRedirect(req SDNRedirect) error {
	// Redirect all interfaces (except those that take part)
	var tables []Table
	for _, r := range req {
		router := utils.RemoveCIDR(cfg.NameToIface[r.RedirTo].NeighborIP)
		rdev := cfg.NameToIface[r.RedirTo].Ifname
		var rules []RedirRule
		domain := utils.DomainFromName(r.Redirect)
		table := "1" + pad(domain)
		for _, iface := range cfg.Interfaces {
			if iface.Neighbor == "" {
				// VLAN
				continue
			} else if iface.Neighbor == r.Redirect {
				// Return path via stateful NFV (XXX: this depends on the type
				// of NFV)
				for _, s := range r.Src {
					rules = append(rules, RedirRule{r.Dst, s, iface.Ifname, table})
				}
				continue
			} else if iface.Neighbor == r.RedirTo {
				continue
			}
			for _, s := range r.Src {
				rules = append(rules, RedirRule{s, r.Dst, iface.Ifname, table})
			}
		}
		tables = append(tables, Table{table, router, rdev, rules})
	}
	return redirect("1", tables)
}

func (c *HostLocal) NFVConfig(req NFVConfig) error {
	// Create & remove containers
	desired := map[string]bool{}
	var tables []Table
	for _, r := range req {
		name := fmt.Sprintf("nfv-as%s-%s", r.Domain, r.Kind)
		desired[name] = true
		ip, err := ctrAdd(CtrRequest{
			Name:  name,
			Image: fmt.Sprintf("vnet-%s", r.Kind),
			Net:   "vc-net",
			Priv:  true,
			Env:   r.Env,
		})
		if err != nil {
			log.Println("Container", name, "failed:", err)
			continue
		}
		log.Println("Container", name, "IP:", ip)
		table := "2" + pad(suffix(ip))
		var src []string
		if len(r.Src) > 0 {
			src = r.Src
		} else {
			src = []string{"172.30.0.0/16"}
		}
		var rules []RedirRule
		// Create rules that act only on ingress interfaces, not on e.g.
		// container interfaces
		for _, iface := range cfg.Interfaces {
			if iface.Neighbor == "" {
				continue
			}
			for _, s := range src {
				rules = append(rules,
					// Traffic from the domain to the Internet
					RedirRule{
						From:  fmt.Sprintf("172.30.%s.0/24", r.Domain),
						To:    s,
						Iif:   iface.Ifname,
						Table: table,
					},
					// Traffic from the Internet to the domain
					RedirRule{
						From:  s,
						To:    fmt.Sprintf("172.30.%s.0/24", r.Domain),
						Iif:   iface.Ifname,
						Table: table,
					},
				)
			}
		}
		tables = append(tables, Table{
			Table:  table,
			Router: ip,
			Rules:  rules,
		})
	}
	// Result: container addresses
	running, err := dockerRunning("nfv-*")
	if err != nil {
		return err
	}
	for _, k := range running {
		if !desired[k] {
			if err := ctrDel(k); err != nil {
				log.Println("Container", k, "delete failed:", err)
			} else {
				log.Println("Container", k, "deleted")
			}
		}
	}
	// Great, now enforce the redirection rules
	redirect("2", tables)
	return nil
}

func (c *HostLocal) ContainerConfig(req ContainerConfig) error {
	// Create & remove containers
	desired := map[string]bool{}
	for _, r := range req.Containers {
		//name := fmt.Sprintf("%s-%s", req.Prefix, r.Name)
		desired[r.Name] = true
		ip, err := ctrAdd(r)
		if err != nil {
			log.Println("Container", r.Name, "failed:", err)
		} else {
			log.Println("Container", r.Name, "IP:", ip)
		}
	} // Result: container addresses
	running, err := dockerRunning(req.Prefix + "-*")
	if err != nil {
		return err
	}
	for _, k := range running {
		if !desired[k] {
			if err := ctrDel(k); err != nil {
				log.Println("Container", k, "delete failed:", err)
			} else {
				log.Println("Container", k, "deleted")
			}
		}
	}
	return nil
}
