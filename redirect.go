package main

// TODO: set pref?

// Table 1XXX: domain redirect
// Table 2XXX: container redirect

import (
	"log"
	"strings"

	"vnet.uvalight.net/utils"
)

type (
	RedirRule struct {
		From, To string
		Iif      string
		Table    string
	}
	Table struct {
		Table  string
		Router string
		Dev    string
		Rules  []RedirRule
	}
)

func (rr RedirRule) String() string {
	s := "from "
	if rr.From == "" || rr.From == "0.0.0.0/0" {
		s += "all"
	} else {
		s += rr.From
	}
	if rr.To != "" && rr.To != "0.0.0.0/0" {
		s += " to " + rr.To
	}
	s += " iif " + rr.Iif + " table " + rr.Table
	return s
}

func pad(s string) string {
	for len(s) < 3 {
		s = "0" + s
	}
	return s
}

func ourTable(s, t string) bool {
	parts := strings.Split(s, " ")
	if len(parts) < 2 || parts[len(parts)-2] != "lookup" {
		println("not lookup")
		return false
	}
	table := parts[len(parts)-1]
	return len(table) == 4 && strings.HasPrefix(table, t)
}

func redirect(t string, tables []Table) error {
	// Rules to keep or create
	create := map[string]bool{}
	keep := map[string]bool{}
	for _, t := range tables {
		for _, r := range t.Rules {
			create[r.String()] = true
		}
		if len(t.Rules) > 0 {
			args := []string{"route", "replace", "default", "via", t.Router}
			if t.Dev != "" {
				args = append(args, "dev", t.Dev)
			}
			args = append(args, "table", t.Table)
			utils.RunLogged("host-netctl", "ip", args, nil)
			keep[t.Table] = true
		}
	}
	existing, _ := getRules()
	for rule, _ := range existing {
		if !create[rule] && ourTable(rule, t) {
			parts := strings.Split(rule, " ")
			table := parts[len(parts)-1]
			args := []string{"rule", "del"}
			args = append(args, parts...)
			utils.RunLogged("host-netctl", "ip", args, nil)
			if !keep[table] {
				utils.RunLogged("host-netctl", "ip", []string{
					"route", "del", "default", "table", table,
				}, nil)
				keep[table] = true
			}
		}
	}
	for rule, _ := range create {
		if !existing[rule] {
			args := []string{"rule", "add"}
			args = append(args, strings.Split(rule, " ")...)
			utils.RunLogged("host-netctl", "ip", args, nil)
		}
	}
	return nil
}

func getRules() (map[string]bool, error) {
	lines, err := utils.CommandLines([]string{"ip", "rule", "show"})
	if err != nil {
		return nil, err
	}
	rules := make(map[string]bool)
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		parts := strings.SplitN(line, ":\t", 2)
		rules[strings.Trim(parts[1], " \t")] = true
	}
	log.Printf("parsed IP rules: %+v", rules)
	return rules, nil
}
